# Installation
Download / copy the script to your scripts folder (to see the path, go to Options -> Processing -> Scripts)

You can also clone this repository into that script directory

For example, on Mac OS X, the directory is `${HOME}/Library/Application Support/QGIS/QGIS3/profiles/default/processing/scripts`.

# Scripts
## kaart_address_deduplication.py
This script attempts to deduplicate addresses
## kaart_signpost_imagery.py
This script creates areas around intersections with imagery. You should have a road network and a road network of roads you are interested in, along with tracks from an imagery provider.
## kaart_street_address_fixup.py
This script is supposed to attempt to use a road network (from an authoritive source, should have diacritics) to modify addr:street tags in an address file so that they match. This currently doesn't work.
## kaart_parallel_line_reduction.py
This script attempts to remove largely parallel lines. Currently a WIP.
