# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterField,
                       QgsProcessingParameterVectorLayer)
from qgis import processing


class StreetAddressFixup(QgsProcessingAlgorithm):

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ExampleProcessingAlgorithm()

    def name(self):
        return 'Street Address Fixup'

    def displayName(self):
        return self.tr('Street Address Fixup')

    def group(self):
        return self.tr('Kaart')

    def groupId(self):
        return 'Kaart'

    def shortHelpString(self):
        return self.tr("Compares names to street names and tries to fix them")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(QgsProcessingParameterVectorLayer('addresses', 'Addresses', types=[QgsProcessing.TypeVector], defaultValue=None))
        self.addParameter(QgsProcessingParameterField('addr:street', 'Street Address', type=QgsProcessingParameterField.Any, parentLayerParameterName='addresses', allowMultiple=False, defaultValue='addr:street'))
        self.addParameter(QgsProcessingParameterVectorLayer('streets', 'Streets', types=[QgsProcessing.TypeVectorLine, QgsProcessing.TypeVectorPolygon], defaultValue=None))
        self.addParameter(QgsProcessingParameterField('street_names', 'Street Names', type=QgsProcessingParameterField.String, parentLayerParameterName='streets', allowMultiple=False, defaultValue='name'))
        self.addParameter(QgsProcessingParameterFeatureSink('fixed_addresses', 'Fixed Addresses', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        feedback = QgsProcessingMultiStepFeedback(3, feedback)

        nameset = set()
        total = 100.0 / parameters['streets'].featureCount() if parameters['streets'].featureCount() else 0
        for current, feature in enumerate(parameters['streets']):
            if feedback.isCanceled():
                break
            nameset.add(feature[parameters['street_names']])
            feedback.setProgress(int(current * total))
        # This does not properly sort diactrics (è and so on)
        namelist = sorted(list(nameset))
        
        feedback.setCurrentStep(1)
        address_dict = {}
        total = 100.0 / parameters['addresses'].featureCount() if parameters['addresses'].featureCount() else 0
        for current, feature in enumerate(parameters['addresses']):
            if feedback.isCanceled():
                break
            addr_street = feature[parameters['addr:street']]
            if addr_street not in address_dict:
                address_dict[addr_street] = []
            address_dict[address_street].append(feature)
            feedback.setProgress(int(current * total))
        
        feedback.setCurrentStep(2)
        
        total = 100.0 / (parameters['addresses'].featureCount() * parameters['streets'].featureCount()) if parameters['addresses'].featureCount() and parameters['streets'].featureCount() else 0
        total_change = 1 / total
        position = 1
        for address in address_dict:
            feedback.setProgress(position * total_change)
            position += 1
            if (feedback.isCanceled()):
                break
            

        return {self.OUTPUT: dest_id}
