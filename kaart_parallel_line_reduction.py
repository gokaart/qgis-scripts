# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsCoordinateReferenceSystem,
                       QgsFeatureSink,
                       QgsGeometry,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber)
from qgis import processing


class SimilarLineProcessing(QgsProcessingAlgorithm):
    """
    This takes a layer, and tries to output only one line per roughly similar lines.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    MAX_DISTANCE = 'MAX_DISTANCE'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return SimilarLineProcessing()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'similarlineprocessing'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Similar line processing (reduction)')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('Kaart')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'Kaart'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("Takes a line layer and removes extraneous lines that largely parallel other lines.")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It must be line geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorLine]
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output layer')
            )
        )

        self.addParameter(QgsProcessingParameterNumber(self.MAX_DISTANCE, self.tr("Maximum distance between lines to be considered")))

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        try:
            self._realProcessAlgorithm(parameters, context, feedback)
        except Exception as e:
            feedback.setInfo("Failed?")

    def _realProcessAlgorithm(self, parameters, context, feedback):
        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        source = self.parameterAsSource(
            parameters,
            self.INPUT,
            context
        )

        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            source.fields(),
            source.wkbType(),
            source.sourceCrs()
        )

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(source.sourceCrs().authid()))

        # If sink was not created, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSinkError method to return a standard
        # helper text for when a sink cannot be evaluated
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))


        # Convert to EPSG:3395
        reprojected_layer = processing.run("native:reprojectlayer", {
            'INPUT': parameters[self.INPUT],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT,
            'TARGET_CRS': QgsCoordinateReferenceSystem('EPSG:3395')}, context=context, feedback=feedback)['OUTPUT']

        # Compute the number of steps to display within the progress bar and
        # get features from source
        total = 100.0 / reprojected_layer.featureCount() if reprojected_layer.featureCount() else 0
        features = reprojected_layer.getFeatures()

        # {feature: [features]}
        max_distance = parameters[self.MAX_DISTANCE]
        to_further_check = {}
        feedback.pushInfo(self.tr("Looking for candidates"))
        for current, feature in enumerate(features):
            # Stop the algorithm if cancel button has been clicked
            if feedback.isCanceled():
                break
            future_check = []
            to_further_check[feature] = future_check
            for feature2 in features:
                if feature.geometry().distance(feature2.geometry()) <= max_distance:
                    future_check.append(feature2)

            # Update the progress bar
            feedback.setProgress(int(current * total))

        feedback.setProgress(0)
        feedback.pushInfo(self.tr("Checking candidates"))
        to_add = []
        total = len(to_further_check)
        for current, feature in enumerate(to_further_check):
            adding = True
            matches = []
            for other_feature in to_further_check[feature]:
                if other_feature in to_add:
                    adding = False
                    break
                good_feature = True
                for point in other_feature.geometry().asPolyline():
                    if feature.geometry().distance(QgsGeometry.fromPointXY(point)) >= max_distance:
                        good_feature = False
                        break
                if good_feature:
                    matches.put(other_feature)
            if adding and len(matches) > 0:
                to_add.put(feature)
            feedback.setProgress(int(current * total))
        for feature in to_add:
            if feedback.isCanceled():
                break
            sink.addFeature(feature, QgsFeatureSink.FastInsert)

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {self.OUTPUT: reprojected_layer}
